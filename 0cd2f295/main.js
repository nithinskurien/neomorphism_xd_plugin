/*
 * Sample plugin scaffolding for Adobe XD.
 *
 * Visit http://adobexdplatform.com/ for API docs and more sample code.
 */


const {Rectangle, Color} = require("scenegraph"); 

function groupHandlerFunction(selection) { 
    
    let intial_selection = selection.items;
    intial_selection.forEach(function (item, i){
    let commands = require("commands");
    selection.items = [item];
    commands.group()
    let group = selection.items[0]
    group.name = item.name
    let parentCenter = item.parent.localCenterPoint;
    let rectMaskDimension = getRectangleMaskDimension(item)
 
    const newElement = new Rectangle(); 
    newElement.name = item.name + " Mask";
    item.name = item.name + " Diffuse";
    newElement.width = rectMaskDimension * 1.5;
    newElement.height = rectMaskDimension * 1.5;
    newElement.fill = new Color("White");
    group.addChild(newElement,0);
    let nodeBounds = newElement.localBounds;
    console.log("The node bound is: " + nodeBounds.x + ", " + nodeBounds.y);
    let nodeTopLeft = {x: nodeBounds.x + newElement.width/2, 
                        y: nodeBounds.y + newElement.height/2};
    console.log("The nodeTopLeft bound is: " + nodeTopLeft.x + ", " + nodeTopLeft.y);
    console.log("The parentCenter bound is: " + parentCenter.x + ", " + parentCenter.y );
    newElement.placeInParentCoordinates(nodeTopLeft, parentCenter);
    selection.items = [item];
    commands.duplicate();

})
}

function getRectangleMaskDimension(child){
    var length = 0;
    if (child.localBounds.width > length){
        length = child.localBounds.width;
	}
    if (child.height > length){
        length = child.localBounds.height;
	}
    return length
}

module.exports = {
    commands: {
        createRectangle: groupHandlerFunction
    }
};
