/*
 * Sample plugin scaffolding for Adobe XD.
 *
 * Visit http://adobexdplatform.com/ for API docs and more sample code.
 */


const {Rectangle, Shadow, Color} = require("scenegraph"); 
var commands = require("commands");

function effectsHandlerFunction(selection) { 
    
    let intial_selection = selection.items;
    var diffuse;
    var negative;
    var near_shadow = ["Near Shadow", 5, 5, 5, "#6F7986", "Multiply"];
    var far_shadow = ["Far Shadow", 10, 10, 20, "#A4AEBB", "Multiply"];
    var rim_light = ["Rim Light", -1, -1, 3, "#FFFFFF", "Screen"];
    var glow = ["Glow Light", -2, -2, 10, "#F4FAFF", "Screen"];
    var shadow = [near_shadow, far_shadow];
    var highlight = [rim_light, glow];
    var group_array1 = [];
    var group_array2 = [];

    intial_selection.forEach(function (item, i){

        item.children.forEach(function (child,i){
            //console.log("The Child is: " + child.name);
            if(child.name.includes("Diffuse")){
                diffuse = child;
                //console.log("The Diffuse is: " + child.name);
		    }
            if(child.name.includes("Subtraction")){
                negative = child;
                negative.name = item.name + " Negation";
                //console.log("The Subtraction is: " + child.name);
		    }
        })
        selection.items = [diffuse, negative];
        var selection_array = [diffuse, negative];
        group_array2 = createEffect(selection, selection_array, highlight, item.name);
        group_array1 = createEffect(selection, selection_array, shadow, item.name);
        selection.items = [item];
        commands.ungroup();
        selection.items = group_array1.concat(group_array2).concat(diffuse);
        commands.group();
        let item_group = selection.items[0]
        item_group.name = item.name;
        selection.items = [negative];
        //console.log("The Edit Context is: " + selection.editContext);
        //commands.ungroup();
        deleteChild(selection, negative);  
})
}

function createEffect(selection, selection_array, effect_array, item_name){
    var group_array = [];
    effect_array.forEach(function (item, i){
        let diffuse_copy;
        let negative_copy; 
        selection.items = selection_array;
        commands.duplicate();

        selection.items.forEach(function (selection_item, j){
            if(selection_item.name.includes("Diffuse")){
                diffuse_copy = selection_item;
            }
            if(selection_item.name.includes("Negation")){
                negative_copy = selection_item;
            }
        })

        negative_copy.fill = new Color("White");
        negative_copy.fillEnabled = true;
        negative_copy.stroke = null;
        negative_copy.strokeEnabled = false;
        negative_copy.shadow = new Shadow(effect_array[i][1], effect_array[i][2], effect_array[i][3], 
                                              new Color(effect_array[i][4]), true);
        selection.items = [diffuse_copy, negative_copy];
        commands.createMaskGroup();
        let group = selection.items[0];
        group.name = item_name + " " + effect_array[i][0];
        group_array.push(group);

    })

    return group_array;
}

function deleteChild(selection, child){
    
    if (child.isContainer){
        selection.items = [child];
        commands.ungroup();
        selection.items.forEach(function (childNode, i) {
            //console.log("Child " + i + " is " + childNode.name);
            deleteChild(selection, childNode);
        });
    }
    else{
        child.removeFromParent();
    }
}

module.exports = {
    commands: {
        createRectangle: effectsHandlerFunction
    }
};
